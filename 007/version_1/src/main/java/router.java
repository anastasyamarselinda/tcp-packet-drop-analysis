import java.util.*;
import java.io.*;
import java.net.*;
import java.util.regex.*;


class router extends Thread {

    static int tot_router = 0;
    Socket receiver;
    int timetolive = 600;
    node my_node;

    public router( int num_router )
    {
        tot_router = num_router;
    }

    private router(node a_node, Socket a_socket)
    {
        this.receiver = a_socket;

    }

    public void launch(node a_switch)
    {
        Thread a_thread = new Thread() {
            public void run() {
                listen(a_switch);
            }
        };
        System.out.printf("****Switch %s launched\n", a_switch.name);
        a_thread.start();
    }

    private void listen(node a_host)
    {
        long start_time = System.currentTimeMillis();
        int timetolive = 120;
        int in_port = a_host.in_port;
        try {
            ServerSocket rcv_socket = new ServerSocket(in_port);

            while( (System.currentTimeMillis() - start_time) / 1000 < timetolive )
            {
                new router(a_host, rcv_socket.accept()).start();
            }
        } catch ( Exception anException) {
            anException.printStackTrace();
        }
    }

    public void run()
    {
        try ( DataInputStream in = new DataInputStream( this.receiver.getInputStream()); )
        {
            String rcv_msg;
            int flag = 0;
            if ( (rcv_msg = in.readLine()) != null ) {
                String[] data = rcv_msg.split("|");
                String packet = data[1]; //data[0] is the intruction
                if(Pattern.matches("P", data[0])) {
                    forward_packet(packet, flag);
                } else if(Pattern.matches("D", data[0])) {
                    flag = 1;
                    forward_packet(packet, flag);
                } else {
                    System.out.println("No matching instruction : " + rcv_msg);
                }
            }
            this.receiver.close();
        }
        catch ( Exception anException) {
            anException.printStackTrace();
        }
    }

    public void forward_packet(String packet, int flag)
    {
        int out_port = get_nexthop(packet);
        if(flag == 0) {
            packet = "P|" + packet;
        } else {
            packet = "R|" + packet;
            try {
                sleep(10);
            } catch ( Exception anException) {
                anException.printStackTrace();
            }

        }
        try {
            Socket xmit_socket = new Socket("localhost", out_port);
            PrintWriter message_out = new PrintWriter(xmit_socket.getOutputStream(), true);
            message_out.println(packet);
            xmit_socket.close();
        } catch ( Exception anException) {
            anException.printStackTrace();
        }
    }

    public int get_nexthop(String packet)
    {
        String[] info = packet.split(" ");
        String dst_name = info[info.length - 1];

        int my_idx = Integer.parseInt(this.my_node.name.substring(2));

        node[] neighbors = my_node.conn_list;
        int len = neighbors.length;

        int dst_idx = Integer.parseInt(dst_name.substring(3));

        int next_hop;

        if( dst_idx > my_idx + tot_router ) {
            // this case cannot happen for the last switch
            assert my_idx < tot_router - 1;
            next_hop = neighbors[len - 1].in_port;
        } else if( dst_idx < my_idx + tot_router ) {
            // this case cannot happen for the first switch
            assert my_idx > 0;
            next_hop = neighbors[0].in_port;
        } else {
            next_hop = (my_idx == 0) ? neighbors[0].in_port : neighbors[1].in_port;
        }
        return next_hop;
    }

}
