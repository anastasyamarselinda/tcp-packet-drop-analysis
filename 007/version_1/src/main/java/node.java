class node
{
    static int tot_num;
    String name;
    String type;
    String ip;
    String mac;
    int in_port; // there are 65535 tcp and 65535 udp ports in total
    String host_name = "localhost";
    int num_conn;
    node[] conn_list;

    public node (String type, int num, int tot)
    {
        this.name = type + num;
        this.type = type;
        this.num_conn = 0;
        tot_num = tot * 2;
        this.conn_list = new node[1];
        if(type == "sw") { // Switch : 651-02 is the number of the course
            this.in_port = 65102 + num;
        }else { // Host : port 1024-49151 are user or registered port
            this.in_port = 49151 + num;
        }
    }

    public void add_connection (node conn)
    {
        if (num_conn == 0) {
            conn_list[num_conn] = conn;
        }
        else {
            node[] temp = conn_list;
            conn_list = new node[temp.length + 1];
            System.arraycopy(temp, 0, conn_list, 0, temp.length);
            conn_list[temp.length] = conn;
        }
        num_conn++;
    }

    public node[] get_connections()
    {
        return this.conn_list;
    }

    public void print_connections()
    {
        String output = this.name + " : | ";
        for(node a_node : conn_list) {
            output += a_node.name + " | ";
        }
        System.out.println(output);
    }
}
