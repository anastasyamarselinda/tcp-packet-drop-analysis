import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.net.*;
import java.util.*;
import java.io.*;

public class monitor
{

    static long timetolive = 600; //10 minutes -> 600

    public void work(int port_number)
    {
        long init_time = System.currentTimeMillis();
        float curr_status = 0;
        // String host_name = args[0];

        try {
            // Socket monitor_agent = new Socket (host_name, port_number);
            Socket monitor_agent = new Socket("localhost", port_number);

            while ( curr_status < timetolive )  {
                //listen to communication



                //return src and dst if rxmition



                // Assume a packet between hst5 and hst8 is captured as retramsmitted.
                String src = "hst5";
                String dst = "hst8";

                // Send to the discovery agent

                try( PrintWriter message = new PrintWriter(monitor_agent.getOutputStream(), true); ) {
                    message.println( src + "|" + dst );
                }

                // Check time to live
                curr_status = (System.currentTimeMillis() - init_time) / 1000;
            }
            monitor_agent.close();
        }
        catch ( Exception anException) {
            anException.printStackTrace();
        }

    }

    public static void main (String[] args)
    {
        long init_time = System.currentTimeMillis();
        float curr_status = 0;
        // String host_name = args[0];
        int port_number = Integer.parseInt(args[1]);

        try {
            // Socket monitor_agent = new Socket (host_name, port_number);
            Socket monitor_agent = new Socket("localhost", port_number);

            while ( curr_status < timetolive )  {
                //listen to communication



                //return src and dst if rxmition



                // Assume a packet between hst5 and hst8 is captured as retramsmitted.
                String src = "hst5";
                String dst = "hst8";

                // Send to the discovery agent

                try( PrintWriter message = new PrintWriter(monitor_agent.getOutputStream(), true); ) {
                    message.println( src + "|" + dst );
                }

                // Check time to live
                curr_status = (System.currentTimeMillis() - init_time) / 1000;
            }
            monitor_agent.close();
        }
        catch ( Exception anException) {
            anException.printStackTrace();
        }
    }

}
