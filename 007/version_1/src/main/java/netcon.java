import java.io.*;
import java.util.Random;
import java.lang.Math;
public class netcon
{
    private static network net;
    private static int[][] flows;
    static int number_switch;


    public static void create_flow(int num_flow)
    {
        flows = new int[num_flow][2];
        for(int index = 0; index < num_flow; index++)
        {
            int origin = new Random().nextInt(number_switch - 1);
            int val = Math.round(number_switch / 3 );
            int destination = new Random().nextInt((number_switch - val - 1) + 1) + val;

            // The condition below aims at preventing a host from sending to itself
            if(origin == destination && origin < number_switch - 1) {
                origin--;
            } else if(origin == destination && origin == 0) {
                origin++;
            }

            flows[index] = new int[] {origin, destination};
            System.out.printf("****Initialized flow from hst%d to hst%d\n", number_switch + origin, number_switch + destination);
        }
    }

    public static void launch_switches()
    {
        router my_router = new router(number_switch);
        node[] switches = new node[number_switch];
        System.arraycopy(net.nodes, 0, switches, 0, number_switch);
        for(int idx = 0; idx < number_switch; idx++) {
            my_router.launch(switches[idx]);
            // new router(number_switch).launch(switches[idx]);
        }
    }

    public static void send_flow()
    {
        // while(true) {
            for(int index = 0; index < flows.length; index++)
            {
                host source = new host();
                String packet = form_packet(flows[index]);
                node[] hosts = new node[] {
                    net.nodes[number_switch + flows[index][0]],
                    net.nodes[number_switch + flows[index][1]]
                };
                System.out.printf("\nSending a packet from %s to %s......\n", hosts[0].name, hosts[1].name);
                source.launch_host(hosts, packet);
            }
        // }
    }

    public static String form_packet(int[] hosts)
    {
        node src = net.nodes[number_switch + hosts[0]];
        node dst = net.nodes[number_switch + hosts[1]];
        int src_port = src.in_port;
        int dst_port = dst.in_port;
        String src_name = src.name;
        String dst_name = dst.name;
        String message = "P|" + String.valueOf(src_port) + " " + String.valueOf(dst_port) + " " + src_name + " " + dst_name;

        return message;
    }

    public static void main(String[] args)
    {
        number_switch = Integer.parseInt(args[0]);
        int num_flow = Integer.parseInt(args[1]);

        System.out.printf("Generating a network of %d switches and %d hosts......\n", number_switch, number_switch);
        net = new network( number_switch );

        System.out.printf("\nGenerating %d flow circuits......\n", num_flow);
        create_flow(num_flow);

        System.out.println("\nLaunching the switches......");
        launch_switches();

        send_flow();
    }

    /**
    // This code is used to replicate a file
    private static void replicate(File origin, File destination)
    {
        InputStream read_in = null;
        OutputStream write_out = null;
        try {
            read_in = new FileInputStream(origin);
            write_out = new FileOutputStream(destination);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = read_in.read(buffer)) > 0) {
                write_out.write(buffer, 0, length);
            }
            read_in.close();
            write_out.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args)
    {
        int num_switch = Integer.parseInt(args[0]);
        int num_host = num_switch;
        net = new network(num_switch);

        // String switch_loc = "./router.java";
        // String host_loc = "./host.java";
        String subnet_loc = "./subnet.java";

        try {

            File switch_file = new File(switch_loc);
            File host_file = new File(host_loc);

            for(int idx = 0; idx < num_switch; idx++) {
                int hst_idx = idx + num_switch;
                File switch_dst = new File("./switch" + idx + ".java");
                File host_dst = new File("./host" + hst_idx + ".java");
                replicate(switch_file, switch_dst);
                replicate(host_file, host_dst);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    */

}
