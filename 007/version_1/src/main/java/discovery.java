import java.util.stream.IntStream;
import java.util.regex.*;
import java.net.*;
import java.util.*;
import java.io.*;

public class discovery extends Thread
{

    static int position;
    static long update_time = 10;
    static long timetolive = 600; //10 minutes
    static int num_node;
    static HashMap<Integer, node[]> curr_paths;
    static int num_call = 0;

    // timer for updates to send
    static long running = 0;
    static String write_lock = "acquired";


    // port_stat
    private static int src_port = 22222;
    private static int dst_port = 22223;
    private static String dst_host = "localhost";

    // each thread own objects
    node[] visited;
    boolean found=false;
    Socket receiver;
    node src;
    node dst;
    node[] path;

    public discovery(){}

    public discovery( Socket receiver )
    {
        this.visited = new node[num_node];
        this.receiver = receiver;
    }

    public void listen()
    {
        // timer for the entire program to live
        long init_time = System.currentTimeMillis();
        long curr_status = 0;

        try {
            ServerSocket rcv_socket = new ServerSocket (src_port);

            while ( curr_status < timetolive )  {

                new discovery( rcv_socket.accept()).start();

                // Check time to live
                curr_status = (System.currentTimeMillis() - init_time) / 1000;
            }

            rcv_socket.close();
        }
        catch ( Exception anException) {
            anException.printStackTrace();
        }

    }

    public void run()
    {
        try ( DataInputStream in = new DataInputStream( this.receiver.getInputStream()); )
        {
            String rcv_msg;
            if ( (rcv_msg = in.readLine()) != null ) {
                String[] data = rcv_msg.split("|");

                this.src = network.get_node(data[0]);
                this.dst = network.get_node(data[1]);

                this.path = get_path();

                synchronized(write_lock)
                {
                    num_call++; //Keep track of the total number of rxmited packet
                    curr_paths.put(num_call, this.path);

                    long curr_time = (System.currentTimeMillis() - running) / 1000;

                    // When it is time to update the analysis agent
                    if(curr_time >= update_time) {
                        to_analysis();
                        running = 0;
                    }
                }
            }
            this.receiver.close();
        }
        catch ( Exception anException) {
            anException.printStackTrace();
        }
    }

    public node[] get_path()
    {
        node[] finalpath;
        traverse(this.src, this.dst);
        finalpath = actualpath(visited,position);
        print_path(finalpath);
        return finalpath;
    }

    public void traverse(node start, node dest)
    {
        node[] checklist = start.get_connections();
        visited[position]=start;
        position++;
        if (start.name == dest.name) {
            found=true;
        }
        else {
            List<node> vis_list = Arrays.asList(visited);
            for (int t = 0; t < checklist.length; t++) {
                if (!vis_list.contains(checklist[t])) {
                    if(found==false) {
                        traverse(checklist[t], dest);
                    }
                }
            }
        }
    }

    public node[] actualpath(node[] traversepath, int actualsz)
    {
        for(int r = actualsz-1; r > 0; r--){
            node[] testconn = traversepath[r].get_connections();
            for(int re = r-1; re >= 0; re--)
            {
                if(checkconn(traversepath[re],testconn)){
                    break;
                } else {
                    traversepath=removenode(traversepath[re],traversepath,actualsz);
                    r--;
                    position--;
                }
            }
        }
        int index = Arrays.asList(traversepath).indexOf(null);
        return Arrays.copyOfRange(traversepath, 0, index);
    }

    public boolean checkconn(node check,node[] list)
    {
        for(int l=0;l<list.length;l++){
            if(check==list[l]){
                return true;
            }
        }
        return false;
    }

    public node[] removenode(node rem,node[] remlst,int sz)
    {
        for(int r=0;r<sz;r++){
            if(rem==remlst[r]){
                for(int tr=r;tr<sz;tr++){
                    remlst[tr]=remlst[tr+1];
                }
            }
        }
        return remlst;
    }

    public void print_path(node[] finalpath)
    {
        String path = "Path : ";
        for(int p = 0; p < position - 1; p++)
        {
            path += finalpath[p].name + " ---> ";
        }
        path += finalpath[position - 1].name;
        System.out.println(path);
    }

    public void to_analysis()
    {

        try {
            Socket xmit_socket = new Socket (dst_host, dst_port);

            DataOutputStream out = new DataOutputStream( xmit_socket.getOutputStream());

            ObjectOutputStream obj_out = new ObjectOutputStream(out);

            obj_out.writeObject(curr_paths);

        } catch ( Exception anException) {
            anException.printStackTrace();
        }

    }

    public static void main (String[] args)
    {

        // timer for the entire program to live
        long init_time = System.currentTimeMillis();
        long curr_status = 0;

        // src_port = Integer.parseInt(args[0]);
        // dst_host = args[1];
        // dst_port = Integer.parseInt(args[2]);

        try {
            ServerSocket rcv_socket = new ServerSocket (src_port);

            while ( curr_status < timetolive )  {

                new discovery( rcv_socket.accept()).start();

                // Check time to live
                curr_status = (System.currentTimeMillis() - init_time) / 1000;
            }

            rcv_socket.close();
        }
        catch ( Exception anException) {
            anException.printStackTrace();
        }
    }

}
