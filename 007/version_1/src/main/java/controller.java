public class controller
{
    private int discovery_port = 22222;
    private int analysis_port = 22223;
    public static void main(String[] args)
    {
        monitor monitor_agent = new monitor();

        discovery discovery_agent = new discovery();
        discovery_agent.listen();

        analysis analysis_agent = new analysis();
        analysis_agent.listen();

        monitor_agent.work(discovery_port);

    }

}
