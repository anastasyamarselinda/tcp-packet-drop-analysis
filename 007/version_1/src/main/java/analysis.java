import java.net.*;
import java.util.*;
import java.io.*;


public class analysis extends Thread
{
    private static int src_port = 22223;
    static Hashtable<link, Double> link_vote;
    static ArrayList<link> worse_links;
    static long timetolive = 600; //10 minutes

    int num_hops;
    node[] curr_path;
    Socket receiver;
    HashMap<Integer, node[]> rxmit_paths;

    public analysis(){}
        
    public analysis(Socket receiver)
    {
        this.receiver = receiver;
    }

    public void listen()
    {
        // timer for the entire program to live
        long init_time = System.currentTimeMillis();
        long curr_status = 0;


        try {
            ServerSocket rcv_socket = new ServerSocket (src_port);

            while ( curr_status < timetolive )  {

                new analysis( rcv_socket.accept()).start();

                // Check time to live
                curr_status = (System.currentTimeMillis() - init_time) / 1000;
            }
            get_worst_link();
            rcv_socket.close();
        }
        catch ( Exception anException) {
            anException.printStackTrace();
        }

    }

    public void run()
    {
        try (DataInputStream in = new DataInputStream( this.receiver.getInputStream());)
        {
            ObjectInputStream in_packet = new ObjectInputStream(in);

            this.rxmit_paths = (HashMap<Integer, node[]>) in_packet.readObject();

        } catch ( Exception anException) {
            anException.printStackTrace();
        }

        for(Map.Entry<Integer, node[]> a_path : this.rxmit_paths.entrySet()){

            path_analysis(a_path);
        }
    }

    public void path_analysis ( Map.Entry<Integer, node[]> a_path )
    {
        this.curr_path  = a_path.getValue();
        this.num_hops = this.curr_path.length - 2;

        for(int index = 0; index < this.curr_path.length; index++)
        {
            node[] nodes = Arrays.copyOfRange(this.curr_path, index, index+2);

            link a_link = new link(nodes);

            double vote;
            if(!link_vote.containsKey(a_link)) {
                vote = (double) 1 / this.num_hops;
            } else {
                double old_vote = link_vote.get(a_link);
                vote = old_vote + (1 / this.num_hops);
            }
            link_vote.put(a_link, vote);
        }
    }

    public static void get_worst_link()
    {
        double max_val = -1;
        for(Map.Entry<link, Double> a_link : link_vote.entrySet()){
            double val = a_link.getValue();
            link a_key = a_link.getKey();
            if(val > max_val) {
                worse_links = new ArrayList<link>();
                max_val = val;
                worse_links.add(a_key);
            } else if(val == max_val) {
                worse_links.add(a_key);
            }
        }
    }

    public static void main (String[] args)
    {
        // timer for the entire program to live
        long init_time = System.currentTimeMillis();
        long curr_status = 0;

        src_port = Integer.parseInt(args[0]);

        try {
            ServerSocket rcv_socket = new ServerSocket (src_port);

            while ( curr_status < timetolive )  {

                new analysis( rcv_socket.accept()).start();

                // Check time to live
                curr_status = (System.currentTimeMillis() - init_time) / 1000;
            }
            get_worst_link();
            rcv_socket.close();
        }
        catch ( Exception anException) {
            anException.printStackTrace();
        }
    }
}

class link {
    node node_one;
    node node_two;

    public link(node[] nodes)
    {
        this.node_one = nodes[0];
        this.node_two = nodes[1];
    }
}
