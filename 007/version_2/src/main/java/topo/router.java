import java.util.*;
import java.io.*;
import java.net.*;
import java.util.regex.*;


class router extends Thread {

    private static network net;
    static int tot_router = 0;
    Socket receiver;
    int timetolive = 10;
    node my_node;
    int prob;

    public router( int num_router )
    {
        tot_router = num_router;
        readNetObject();
    }

    private router(node a_node, Socket a_socket, int probability)
    {
        this.receiver = a_socket;
        this.my_node = a_node;
        this.prob = probability;
    }

    public void launch(node a_switch)
    {
        Thread a_thread = new Thread() {
            public void run() {
                listen(a_switch);
            }
        };
        System.out.printf("*******Switch %s launched\n", a_switch.name);
        a_thread.start();
    }

    private void listen(node a_switch)
    {
        long start_time = System.currentTimeMillis();
        int timetolive = 120;
        int in_port = a_switch.in_port;
        try {
            ServerSocket rcv_socket = new ServerSocket(in_port);

            while((System.currentTimeMillis() - start_time) / 1000 < timetolive)
            {
                //Always make switch #1 have a 1 in 2 chance of dropping the packet
                if(a_switch.name.contains("1")){
                    new router(a_switch, rcv_socket.accept(), 2).start();
                }
                else {
                    //Otherwise the switch will have a 1 in 10 chance of dropping the packet
                    new router(a_switch, rcv_socket.accept(), 10).start();
                }
            }
        } catch ( Exception anException) {
            anException.printStackTrace();
        }
    }

    public void run()
    {
        try ( DataInputStream in = new DataInputStream(
                                            this.receiver.getInputStream()); )
        {
            String rcv_msg;
            int flag = 0;
            if ( (rcv_msg = in.readLine()) != null ) {
                String[] data = rcv_msg.split("\\|");
                String packet = data[1]; //data[0] is the intruction
                if(Pattern.matches("P", data[0])) {
                    forward_packet(packet, flag);
                } else if(Pattern.matches("D", data[0])) {
                    System.out.println("Switch: No action needed, dropped packet received : " + rcv_msg);
                } else {
                    System.out.println("Switch: No matching instruction : " + rcv_msg);
                }
            }
            this.receiver.close();
        }
        catch ( Exception anException) {
            anException.printStackTrace();
        }
    }

    public void forward_packet(String packet, int flag)
    {
        // need to incorporate the idea of changing the dst into src addr, as well as the idea of routing table
        int out_port = get_nexthop(packet);
        boolean willDrop = (Math.random() * this.prob) > (this.prob/2);
        if(!willDrop) {
            packet = "P|" + packet;
        } else {
            System.out.println("Switch: PACKET DROP, " + packet);
            try {
                packet = "D|" + packet;
            } catch ( Exception anException) {
                anException.printStackTrace();
            }

        }
        try {
            Socket xmit_socket = new Socket("localhost", out_port);
            PrintWriter message_out = new PrintWriter(
                                        xmit_socket.getOutputStream(), true);
            message_out.println(packet);
            xmit_socket.close();
            if(willDrop){
                //If the packet was dropped, wait 1 second and then retransmit
                sleep(1000);
                packet = packet.replace("D|","");
                packet = "P|" + packet;
                xmit_socket = new Socket("localhost", out_port);
                message_out = new PrintWriter(
                        xmit_socket.getOutputStream(), true);
                message_out.println(packet);
                xmit_socket.close();
            }

        } catch ( Exception anException) {
            anException.printStackTrace();
        }
    }

    public int get_nexthop(String packet)
    {
        String[] info = packet.split(" ");
        String dst_name = info[info.length - 1];

        int my_idx = Integer.parseInt(this.my_node.name.substring(2));

        node[] neighbors = my_node.conn_list;
        int len = neighbors.length;

        int dst_idx = Integer.parseInt(dst_name.substring(3));

        // System.out.println("Switch: " + my_idx + " *** " + dst_idx);

        int next_hop;

        if( dst_idx > my_idx + tot_router ) {
            // this case cannot happen for the last switch
            assert my_idx < tot_router - 1;
            next_hop = neighbors[len - 1].in_port;
        } else if( dst_idx < my_idx + tot_router ) {
            // this case cannot happen for the first switch
            assert my_idx > 0;
            next_hop = neighbors[0].in_port;
        } else {
            next_hop=(my_idx == 0) ? neighbors[0].in_port : neighbors[1].in_port;
        }
        return next_hop;
    }

    public static void readNetObject()
    {
        try {
            FileInputStream in = new FileInputStream("./netObject");
            ObjectInputStream object = new ObjectInputStream(in);
            net = (network) object.readObject();
            object.close();
            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
