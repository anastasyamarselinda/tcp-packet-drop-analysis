import java.util.regex.*;
import java.util.*;
import java.io.Serializable;

/**
    This network generation code is designed to create a linear topology, where
    n switches are interconnected and each switch is connected to one host.

    The user is recommended to parse in command line the desired number of switch.

    An example of instruction set to run this program for 10 switches is:
        javac network.java
        java network 10
*/
public class network implements Serializable {

    static node[] nodes;
    int[][] adj_mat;
    int num_node;

    public network(int num_switches)
    {
        this.num_node = num_switches * 2;
        this.adj_mat = new int[this.num_node][this.num_node];
        this.nodes = new node[num_node];
    	build_net();
    }

    private void build_net()
    {
        int num_sw = (this.num_node / 2) - 1;
        for (int index = 0; index <= num_sw; index++) {
            node sw_node;
            if (index == 0) {
                sw_node = new node("sw", index, num_sw);
                nodes[index] = sw_node;
            } else {
                sw_node = nodes[index];
            }
            node nxtsw_node = new node("sw", index + 1, num_sw);
            node hst_node = new node("hst", index + num_sw + 1, num_sw);
            nodes[index + num_sw + 1] = hst_node;
            add_link(index, index + num_sw + 1);
            sw_node.add_connection(hst_node);
            hst_node.add_connection(sw_node);

            if (index < num_sw) {
                nodes[index + 1] = nxtsw_node;
                add_link(index, index + 1);
                sw_node.add_connection(nxtsw_node);
                nxtsw_node.add_connection(sw_node);
            }
        }
        net();
    }

    private void add_link(int one, int two)
    {
		this.adj_mat[one][two] = 1;
	    this.adj_mat[two][one] = 1;
    }

    public void net()
    {
        for(node a_node : nodes) {
            a_node.print_connections();
        }
    }

    public static node get_node(String name)
    {
        Pattern pat = Pattern.compile("\\d+");
        Matcher mat = pat.matcher(name);
        int index = -1;
        if (mat.find()) {
            index = Integer.valueOf( mat.group(0) );
        }
        return nodes[index];
    }

    public void print_graph()
    {
	    for (int i = 0; i < this.adj_mat.length; i++) {

		    for (int j = 0; j < this.adj_mat.length; j++) {

                if ( this.adj_mat[i][j] == 1 &&
                            Pattern.matches("sw.*", this.nodes[i].name) ) {
                    System.out.println(this.nodes[i].name + " ---- " +
                                                        this.nodes[j].name);
                }
		    }
	    }
    }

    public static void main(String[] args)
    {
    	network net = new network( Integer.parseInt(args[0]) );
    	// net.print_graph();
    }
}
