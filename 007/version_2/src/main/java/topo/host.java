import java.io.*;
import java.util.*;
import java.net.*;

class host extends Thread
{
    private Socket receiver = null;
    private Socket xmiter = null;
    int timetolive = 600;
    int out_port;
    node next_hop;
    String packet;
    int task = 0; // 0 for send and 1 for receive
    node my_node;
    boolean received = false;

    public host(){}

    private host(node a_node, int task, Socket a_socket, String message)
    {
        try {
            this.task = task;
            this.my_node = a_node;
            if (task == 1) {
                this.receiver = a_socket;
            } else {
                this.next_hop = a_node.conn_list[0];
                this.out_port = this.next_hop.in_port;
                this.xmiter = new Socket("localhost", this.out_port);
                this.packet = message;
            }
        } catch ( Exception anException) {
            anException.printStackTrace();
        }
    }

    public void launch_host(node[] hosts, String message)
    {
        Thread a_thread = new Thread() {
            public void run() {
                launch(hosts[0], message, 0);
                listen(hosts[1], 1);
            }
        };
        a_thread.start();
    }

    private void launch(node a_host, String message, int task)
    {
        Socket temp = null;
        host src_thread = new host(a_host, task, temp, message);
        src_thread.start();
    }

    private void listen(node a_host, int task)
    {
        long start_time = System.currentTimeMillis();
        int timetolive = 10;
        int in_port = a_host.in_port;
        try {
            ServerSocket rcv_socket = new ServerSocket(in_port);
            while((System.currentTimeMillis() - start_time)/1000 < timetolive)
            {
                new host(a_host, task, rcv_socket.accept(), "listener").start();
            }
        } catch(java.net.BindException e) {
            // System.out.println("Host: Port already in use");
        } catch ( Exception anException) {
            anException.printStackTrace();
        }
    }

    public void run()
    {
        if(this.task == 0) {
            send_packet();
        } else {
            receive_packet();
        }
    }

    private void send_packet()
    {
        try {
            PrintWriter message_out = new PrintWriter(
                                        this.xmiter.getOutputStream(), true);
            message_out.println(this.packet);
            this.xmiter.close();
        } catch ( Exception anException) {
            anException.printStackTrace();
        }
        System.out.printf("\n>>> %s: Packet transmited to switch %s through port %d......\n",
                        this.my_node.name, this.next_hop.name, this.out_port);
    }

    private void receive_packet()
    {
        try ( DataInputStream in = new DataInputStream(
                                        this.receiver.getInputStream()); )
        {
            String rcv_msg;
            if ( (rcv_msg = in.readLine()) != null ) {
                String[] data = rcv_msg.split("\\|");
                String packet = data[1]; //data[0] is the intruction
                String[] info = packet.split(" ");
                this.received = true;
                System.out.printf("\n\t<<< %s: Reception of packet transmited from %s through its port %s......\n",
                                this.my_node.name, info[info.length - 2], info[info.length - 3]);
            }
            this.receiver.close();
        }
        catch ( Exception anException) {
            anException.printStackTrace();
        }
    }
}
