package sys;

import topo.node;

import java.lang.reflect.Array;
import java.net.*;
import java.util.*;
import java.io.*;


public class analysis
{
    private static final int analysis_port = 22223;
    // variables for links analysis
     Hashtable<String, Double> link_vote = new Hashtable<String, Double>();
    ArrayList<String> worse_links = new ArrayList<String>();
    ArrayList<String> most_prob_links = new ArrayList<String>();
     double tot_link_vote = 0;

    //variables for switch analysis
    Hashtable<String, Double> switch_vote = new Hashtable<String,Double>();
     ArrayList<String> worse_switches = new ArrayList<String>();
    ArrayList<String> most_prob_switches = new ArrayList<String>();;
    double tot_switch_vote = 0;

    static long timetolive = 600; //10 minutes

    int num_hops;
    node[] curr_path;
    Socket receiver;
    HashMap<Integer, node[]> rxmit_paths;

    public analysis(){}

    public analysis(Socket receiver)
    {
        this.receiver = receiver;
    }

    public void listen()
    {
        System.out.println("Analysis listening....");
        // timer for the entire program to live
        long init_time = System.currentTimeMillis();
        long curr_status = 0;
        long buffer = 0;

        __init__();
        try {
            ServerSocket rcv_socket = new ServerSocket (analysis_port);

            while ( curr_status < timetolive )  {

                run( rcv_socket.accept().getInputStream() );

                // Check time to live
                curr_status = (System.currentTimeMillis() - init_time) / 1000-buffer;
                System.out.println(curr_status);
                if(curr_status >= 60 ) {
                    get_worst_link();
                    get_worst_switch();
                    print_results();
                    buffer = buffer+60;
                    __init__();
                }
            }
            rcv_socket.close();
        }
        catch ( Exception anException) {
            anException.printStackTrace();
        }

    }

    public void __init__()
    {
        node[] nodes = new node[]{new node(null, 0, 0), new node(null, 0, 0)};
        link back_pointer = new link(nodes);
        worse_links = new ArrayList<String>();
    }

    public void run(InputStream is)
    {
        try (DataInputStream in = new DataInputStream( is);)
        {
            System.out.println("Received by analysis!");
            ObjectInputStream in_packet = new ObjectInputStream(in);

            this.rxmit_paths = (HashMap<Integer, node[]>) in_packet.readObject();

        } catch ( Exception anException) {
            anException.printStackTrace();
        }

        for(Map.Entry<Integer, node[]> a_path : this.rxmit_paths.entrySet()){
            path_analysis(a_path);
        }
        System.out.println("Analysis completed!!!!!");
    }

    public void path_analysis ( Map.Entry<Integer, node[]> a_path )
    {
        this.curr_path  = a_path.getValue();
        this.num_hops = this.curr_path.length - 2;

        for(int index = 0; index < this.curr_path.length-1; index++)
        {
            node[] nodes = Arrays.copyOfRange(this.curr_path, index, index+2);
            link a_link = new link(nodes);
            evaluate_links(a_link);
        }
        evaluate_switches(this.curr_path);
    }

    public void evaluate_links(link a_link)
    {
        double vote;
        if(!link_vote.containsKey(a_link.to_String())) {
            vote = (double) 1 / this.num_hops;
        } else {
            double old_vote = link_vote.get(a_link.to_String());
            vote = old_vote + (double)1 / this.num_hops;
        }
        tot_link_vote += (double) 1 / this.num_hops;
        link_vote.put(a_link.to_String(), vote);
    }

    public void evaluate_switches(node[] nodes)
    {
        double vote;
        for(node a_node : nodes) {
            if(a_node.type.equals("sw")) {
                if(!switch_vote.containsKey(a_node.to_String())) {
                    vote = (double) 1 / nodes.length;
                } else {
                    double old_vote = switch_vote.get(a_node.to_String());
                    vote = old_vote +  (double)1 / nodes.length;
                }
                tot_switch_vote += (double) 1 / nodes.length;
                switch_vote.put(a_node.to_String(), vote);
            }
        }
    }

    public void get_worst_link()
    {
        double max_val = 0;
        ArrayList<String> max_keys = new ArrayList<String>();
        for(Map.Entry<String, Double> a_link : link_vote.entrySet()){
            double val = a_link.getValue();
            String a_key = a_link.getKey();
            if(val > max_val) {
                max_keys.clear();
                max_keys.add(a_key);
                max_val = val;
            }
            else if(val==max_val){
                max_keys.add(a_key);
                max_val = val;
            }
        }
        for(int i=0;i<max_keys.size();i++) {
            String max_key = max_keys.get(i);
            if (!worse_links.contains(max_key)) {
                worse_links.add(0, max_key);
            }
        }
    }

    public void get_worst_switch()
    {
        double max_val = 0;
        ArrayList<String> max_keys = new ArrayList<String>();
        for(Map.Entry<String, Double> a_node : switch_vote.entrySet()){
            double val = a_node.getValue();
            String a_key = a_node.getKey();
            if(val > max_val) {
                max_keys.clear();
                max_keys.add(a_key);
                max_val = val;
            }
            else if(val==max_val){
                max_keys.add(a_key);
                max_val = val;
            }
        }
        for(int i=0;i<max_keys.size();i++) {
            String max_key = max_keys.get(i);
            if (!worse_switches.contains(max_key)) {
                worse_switches.add(0, max_key);
            }
        }
    }

    public void print_results()
    {
        System.out.println("List of worse links :");
        for(int ind = 0; ind < worse_links.size() ; ind++)
        {
            System.out.println("\t>>> " + worse_links.get(ind));
        }

        System.out.println("List of worse switches :");
        for(int ind = 0; ind < worse_switches.size() ; ind++)
        {
            System.out.println("\t>>> " + worse_switches.get(ind));
        }
    }
}

class link {
    node node_one;
    node node_two;

    public link(node[] nodes)
    {
        this.node_one = nodes[0];
        this.node_two = nodes[1];
    }

    public String to_String()
    {
        return this.node_one.to_String() + " -- " + this.node_two.to_String();
    }
}
