package sys;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.net.*;
import java.util.*;
import java.io.*;

import static java.lang.Thread.sleep;

public class monitor
{
    private static final int discovery_port = 22222;
    static long timetolive = 600; //10 minutes -> 600
    Socket monitor_agent;
    private static String hexToAscii(String hexStr) {
        StringBuilder output = new StringBuilder("");

        for (int i = 0; i < hexStr.length(); i += 2) {
            String str = hexStr.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
        }

        return output.toString();
    }
    public void work() throws InterruptedException {
        long init_time = System.currentTimeMillis();
        float curr_status = 0;
        try {

            try {
                //Run command and read output
                String cmd = "C:\\Program Files\\Wireshark\\tshark.exe -l -i9 -Tfields -edata";
                Process proc = Runtime.getRuntime().exec(cmd);
                String line;
                BufferedReader input = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                //Hangs on this line unless I lock my computer screen or I enter through debug mode?
                //Not sure whats causing the issue, might be my PC
                //sleep(5000);
                while((line=input.readLine()) != null){
                    //System.out.println("got line");
                    if (line.contains("7c3")) {
                        line = hexToAscii(line);
                        line = line.replace("\r", "");
                        line = line.replace("\n", "");
                        //System.out.println("MONITOR: SEEN PACKET '" + line + "'");
                        String[] sections = line.split(" ");
                        try{
                            String src = sections[2];
                            String dst = sections[3];
                            if (line.contains("D|")) {
                                // Send to the discovery agent
                                if(monitor_agent == null || monitor_agent.isClosed()) {
                                    monitor_agent = new Socket("localhost", discovery_port);
                                }
                                try (PrintWriter message = new PrintWriter(monitor_agent.getOutputStream(), true);) {
                                    System.out.println("Sending to discovery........");
                                    message.println(src + "|" + dst);
                                    System.out.println("Sent to discovery!");
                                }
                                //monitor_agent.close();
                            }
                        }
                        catch (ArrayIndexOutOfBoundsException oobe){
                            continue;
                        }
                    }
                }
            }
            catch (Exception err) {
                err.printStackTrace();
                System.out.println("error");
            }
                // Check time to live
                curr_status = (System.currentTimeMillis() - init_time) / 1000;
            // }
        }
        catch ( Exception anException) {
            anException.printStackTrace();
        }
    }
}
