package sys;

import topo.network;
import topo.node;

import java.util.stream.IntStream;
import java.util.regex.*;
import java.net.*;
import java.util.*;
import java.util.Queue;
import java.util.LinkedList;
import java.io.*;
import java.lang.Math;

public class discovery extends Thread
{

    static int position;
    static int sz = 0;
    static long update_time = 30;
    static long timetolive = 600; //10 minutes
    static int num_node;
    static HashMap<Integer, node[]> curr_paths = new HashMap<Integer,node[]>();
    static int num_call = 0;

    // timer for updates to send
    static long init_time = System.currentTimeMillis();
    static long curr_status = 0;
    static long buffer = 0;
    static String write_lock = "acquired";


    // port_stat
    private static final int discovery_port = 22222;
    private static final int analysis_port = 22223;

    // each thread own objects
    node[] visited;
    boolean found=false;
    Socket receiver;
    node src;
    node dst;
    node[] path;

    network net;

    public discovery(Socket receiver)
    {
        this.receiver = receiver;
        readNetObject();
        num_node = net.nodeCount();
        this.visited = new node[num_node];
    }

    public void readNetObject()
    {
        try {
            FileInputStream in = new FileInputStream("./netObject");
            ObjectInputStream object = new ObjectInputStream(in);
            this.net = (network) object.readObject();
            object.close();
            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void listen()
    {
        System.out.println("Discovery listening............");
        // timer for the entire program to live
        long init_time = System.currentTimeMillis();
        long curr_status = 0;

        try {
            ServerSocket rcv_socket = new ServerSocket (discovery_port);

            while ( curr_status < timetolive )  {

                new discovery(rcv_socket.accept()).start();
                // Check time to live
                curr_status = (System.currentTimeMillis() - init_time) / 1000;
            }
            rcv_socket.close();
        }
        catch ( Exception anException) {
            anException.printStackTrace();
        }

    }

    public void run()
    {
        try ( DataInputStream in = new DataInputStream( this.receiver.getInputStream()); )
        {
            System.out.println("Received by discovery!");
            String rcv_msg;
            if ( (rcv_msg = in.readLine()) != null ) {

                String[] data = rcv_msg.split("\\|");
                System.out.println(Arrays.toString(data));
                this.src = this.net.get_node(data[0]);
                this.dst = this.net.get_node(data[1]);

                if(Math.random() < 0.5) {
                    this.path = get_path_dfs();
                } else {
                    this.path = get_path_bfs();
                }

                synchronized(write_lock)
                {
                    num_call++; //Keep track of the total number of rxmited packet
                    curr_paths.put(num_call, this.path);

                    //long curr_time = (System.currentTimeMillis() - running) / 1000;
                    curr_status = (System.currentTimeMillis() - init_time) / 1000 - buffer;
                    // When it is time to update the analysis agent
                    //System.out.println(curr_time);
                    System.out.println(curr_status);
                    if(curr_status >= update_time) {
                        to_analysis();
                        buffer = buffer + 30;
                    }
                }
            }
            this.receiver.close();
        }
        catch ( Exception anException) {
            anException.printStackTrace();
        }
    }

    public node[] get_path_dfs()
    {
        node[] finalpath;
        traverse_dfs(this.src, this.dst, this.visited);
        finalpath = actualpath(this.visited,position);
        print_path(finalpath);
        return finalpath;
    }

    public void traverse_dfs(node start, node dest, node[] visited)
    {
        node[] checklist = start.get_connections();
        visited[position]=start;
        position++;
        if (start.name == dest.name) {
            found=true;
        }
        else {
            List<node> vis_list = Arrays.asList(visited);
            for (int t = 0; t < checklist.length; t++) {
                if (!vis_list.contains(checklist[t])) {
                    if(found==false) {
                        traverse_dfs(checklist[t], dest, visited);
                    }
                }
            }
        }
    }

    public node[] get_path_bfs()
    {
        node[] finalpath;
        traverse_bfs(this.src, this.dst, this.visited);
        finalpath = actualpath(visited, sz);
        print_path(finalpath);
        return finalpath;
    }

    public void traverse_bfs(node start, node dest, node[] visited)
    {
        Queue<node> node_queue = new LinkedList<>();
        node_queue.add(this.src);
        while(node_queue.size() > 0) {
            node current = node_queue.remove();
            current.print_connections();
            node[] checklist = current.get_connections();
            visited[sz] = current;
            sz++;
            if (current.name == dest.name) {
                break;
            } else {
                List<node> vis_list = Arrays.asList(visited);
                for (int t = 0; t < checklist.length; t++) {
                    if (!vis_list.contains(checklist[t])) {
                        node_queue.add(checklist[t]);
                    }
                }
            }
        }
    }

    public node[] actualpath(node[] traversepath, int actualsz)
    {
        System.out.println(actualsz);
        for(int r = actualsz-1; r > 0; r--){
            node[] testconn = traversepath[r].get_connections();
            for(int re = r-1; re >= 0; re--)
            {
                if(checkconn(traversepath[re],testconn)){
                    break;
                } else {
                    traversepath=removenode(traversepath[re],traversepath,actualsz);
                    r--;
                    position--;
                }
            }
        }
        int index = Arrays.asList(traversepath).indexOf(null);
        return Arrays.copyOfRange(traversepath, 0, index);
    }

    public boolean checkconn(node check,node[] list)
    {
        for(int l=0;l<list.length;l++)
        {
            if(check==list[l]) {
                return true;
            }
        }
        return false;
    }

    public node[] removenode(node rem,node[] remlst,int sz)
    {
        for(int r=0; r<sz; r++)
        {
            if(rem == remlst[r]) {
                for(int tr=r;tr<sz;tr++)
                {
                    remlst[tr]=remlst[tr+1];
                }
            }
        }
        return remlst;
    }

    public void print_path(node[] finalpath)
    {
        String path = "Path : ";
        for(int p = 0; p < finalpath.length - 1; p++)
        {
            path += finalpath[p].name + " ---> ";
        }
        path += finalpath[finalpath.length - 1].name;
        System.out.println(path);
    }

    public void to_analysis()
    {
        try {
            System.out.println("Sending to analysis....");
            Socket xmit_socket = new Socket ("localhost", analysis_port);

            DataOutputStream out = new DataOutputStream( xmit_socket.getOutputStream());

            ObjectOutputStream obj_out = new ObjectOutputStream(out);

            obj_out.writeObject(curr_paths);
            System.out.println("Sent to analysis!");

        } catch ( Exception anException) {
            anException.printStackTrace();
        }
    }
}
