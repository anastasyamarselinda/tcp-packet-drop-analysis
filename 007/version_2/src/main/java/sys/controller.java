package sys;

import topo.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class controller extends Thread
{
    public static void main(String[] args) throws IOException, InterruptedException {

        Thread d_g = new Thread( new Runnable() {
            public void run()
            {
                discovery discovery_agent = new discovery();
                discovery_agent.listen();
            }
        });

        Thread a_g = new Thread( new Runnable() {
            public void run()
            {
                analysis analysis_agent = new analysis();
                analysis_agent.listen();
            }
        });
        Thread m_g = new Thread( new Runnable() {
            public void run()
            {
                monitor monitor_agent = new monitor();
                try {
                    monitor_agent.work();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        d_g.start();
        a_g.start();
        m_g.start();
    }
}
