import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.*;

public class path_discovery {

    static int num_node;
    static int position;
    node[] visited;
    boolean found=false;


    public path_discovery()
    {
        visited = new node[num_node];
    }

    public boolean checkconn(node check,node[] list)
    {
        for(int l=0;l<list.length;l++){
            if(check==list[l]){
                return true;
            }
        }
        return false;
    }

    public node[] removenode(node rem,node[] remlst,int sz)
    {
        for(int r=0;r<sz;r++){
            if(rem==remlst[r]){
                for(int tr=r;tr<sz;tr++){
                    remlst[tr]=remlst[tr+1];
                }
            }
        }
        return remlst;
    }

    public node[] actualpath(node[] traversepath, int actualsz)
    {
        for(int r = actualsz-1; r > 0; r--){
            node[] testconn = traversepath[r].get_connections();
            for(int re = r-1; re >= 0; re--)
            {
                if(checkconn(traversepath[re],testconn)){
                    break;
                } else {
                    traversepath=removenode(traversepath[re],traversepath,actualsz);
                    r--;
                    position--;
                }
            }
        }
        int index = Arrays.asList(traversepath).indexOf(null);
        return Arrays.copyOfRange(traversepath, 0, index);
    }

    public boolean isvisited(node n)
    {
        for(int i=0;i<visited.length;i++){
            if(visited[i]==n){
                return true;
            }
        }
        return false;
    }

    public void traverse(node start, node dest)
    {
        node[] checklist = start.get_connections();
        visited[position]=start;
        position++;
        if (start.name == dest.name) {
            found=true;
        }
        else {
            List<node> vis_list = Arrays.asList(visited);
            for (int t = 0; t < checklist.length; t++) {
                if (!vis_list.contains(checklist[t])) {
                    if(found==false) {
                        traverse(checklist[t], dest);
                    }
                }
            }
        }
    }

    public void performDFS(node src, node dest)
    {
        node[] finalpath;
        traverse(src, dest);
        finalpath = actualpath(visited,position);
        print_path(finalpath);
    }

    public void print_path(node[] finalpath)
    {
        String path = "Path : ";
        for(int p = 0; p < position - 1; p++)
        {
            path += finalpath[p].name + " ---> ";
        }
        path += finalpath[position - 1].name;
        System.out.println(path);
    }

    public static void main(String[] args) {

        network net = new network( Integer.parseInt(args[0]) ); //pass num switch
        num_node = net.nodes.length;
        path_discovery rxmit_path = new path_discovery( );

        System.out.println("Printing the network as a graph......");
        net.print_graph();

        System.out.println("\nPrinting the network as an adjacency list......");
        for(node a_node : net.nodes) {
            a_node.print_connections();
        }

        System.out.printf("\nChecking path from %s to %s......%n", net.nodes[6].name, net.nodes[8].name);
        rxmit_path.performDFS(net.nodes[6],net.nodes[8]);
    }
}
