
import java.util.ArrayList;
import java.util.regex.Pattern;

public class Pathanalysis {
    //ArrayList<ArrayList> vertexlist=new ArrayList<ArrayList>();
    node[] visited=new node[20];
    static int position;
    static node[] mainlist=new node[20];
    boolean found=false;

    public void setup(){
        /*for(int i=0;i<numofV;i++){
            vertexlist.add( new ArrayList<Integer>());
        }*/

        mainlist[0]=new node("sw",0,4);
        mainlist[1]=new node("sw",1,4);
        mainlist[2]=new node("sw",2,4);
        mainlist[3]=new node("sw",3,4);
        mainlist[4]=new node("sw",4,4);
        mainlist[5]=new node("hst",5,4);
        mainlist[6]=new node("hst",6,4);
        mainlist[7]=new node("hst",7,4);
        mainlist[8]=new node("hst",8,4);
        mainlist[9]=new node("hst",9,4);

        mainlist[0].add_connection(mainlist[5]);
        mainlist[0].add_connection(mainlist[1]);
        mainlist[1].add_connection(mainlist[0]);
        mainlist[1].add_connection(mainlist[6]);
        mainlist[1].add_connection(mainlist[2]);
        mainlist[2].add_connection(mainlist[1]);
        mainlist[2].add_connection(mainlist[7]);
        mainlist[2].add_connection(mainlist[3]);
        mainlist[3].add_connection(mainlist[2]);
        mainlist[3].add_connection(mainlist[8]);
        mainlist[3].add_connection(mainlist[4]);
        mainlist[4].add_connection(mainlist[3]);
        mainlist[4].add_connection(mainlist[9]);
        mainlist[5].add_connection(mainlist[0]);
        mainlist[6].add_connection(mainlist[1]);
        mainlist[7].add_connection(mainlist[2]);
        mainlist[8].add_connection(mainlist[3]);
        mainlist[9].add_connection(mainlist[4]);
    }

    /*public void edges(int beg, int end){
        (vertexlist.get(beg)).add(end);
        (vertexlist.get(end)).add(beg);
    }*/

    public boolean checkconn(node check,node[] list){
        for(int l=0;l<list.length;l++){
            if(check==list[l]){
                return true;
            }
        }
        return false;
    }

    public node[] removenode(node rem,node[] remlst,int sz){
        //int siz=remlst.length;
        //node[] temp= new node[siz-1];
        for(int r=0;r<sz;r++){
            if(rem==remlst[r]){
                for(int tr=r;tr<sz;tr++){
                    remlst[tr]=remlst[tr+1];
                }
            }
        }
        return remlst;
    }

    public node[] actualpath(node[] traversepath, int actualsz){
        //System.out.println(traversepath.length);
        for(int r=actualsz-1;r>0;r--){
            node[] testconn=traversepath[r].get_connections();
            for(int re=r-1;re>=0;re--) {
                    if(checkconn(traversepath[re],testconn)){
                        break;
                    }
                else{
                    traversepath=removenode(traversepath[re],traversepath,actualsz);
                    r--;
                    position--;
                }
            }
        }
        return traversepath;
    }

    public boolean isvisited(node n){
        for(int i=0;i<visited.length;i++){
            if(visited[i]==n){
                return true;
            }
        }
        return false;
    }

    public void traverse(node start, node dest){
        node[] checklist=start.get_connections();
        visited[position]=start;
        position++;
        //System.out.println(visited);
        if (start.name.equals(dest.name)) {
            found=true;
        }
        else {
            for (int t = 0; t < checklist.length; t++) {
                if (!(isvisited(checklist[t]))) {
                    if(found==false) {
                        traverse(checklist[t], dest);
                    }
                }
            }
        }
    }

    public void performDFS(node src,node dest){
        node[] finalpath;
        //ArrayList<Integer> current=vertexlist.get(src);
        traverse(src,dest);
        System.out.println("THe dfs traversal is");
        for(int p=0;p<position;p++){
            System.out.print(visited[p].name+" ");
        }
        System.out.println("");

        finalpath=actualpath(visited,position);
        System.out.println("The actual path is:");
        //System.out.println(finalpath.length);
        for(int p=0;p<position;p++){
            System.out.print(finalpath[p].name+" ");
        }

    }

    public static void main(String[] args) {
	    Pathanalysis obj=new Pathanalysis();
	    obj.setup();
        /*
	    //client to server links
	    obj.edges(0,5);
        obj.edges(1,6);
        obj.edges(2,7);
        obj.edges(3,8);
        obj.edges(4,9);

        //server to server links
        obj.edges(5,6);
        obj.edges(6,7);
        obj.edges(7,8);
        obj.edges(8,9);

        //To verify the adjacency list, uncomment below code
        for(int i=0;i<vertexlist.size();i++){
            System.out.println(vertexlist.get(i));
        }
        */
        obj.performDFS(mainlist[6],mainlist[8]);


    }
}

class node
{
    static int tot_num;
    String name;
    String type;
    int num_conn;
    node[] conn_list;

    public node (String type, int num, int tot)
    {
        this.name = type + num;
        this.type = type;
        this.num_conn = 0;
        tot_num = tot * 2;
        this.conn_list = new node[1];
    }

    public void add_connection (node conn)
    {
        if (num_conn == 0) {
            conn_list[num_conn] = conn;
        }
        else {
            node[] temp = conn_list;
            conn_list = new node[temp.length + 1];
            System.arraycopy(temp, 0, conn_list, 0, temp.length);
            conn_list[temp.length] = conn;
        }
        num_conn++;
    }

    public node[] get_connections()
    {
        return this.conn_list;
    }

    public void print_connections()
    {
        if ( Pattern.matches("sw.*", this.name) ) {
            String output = this.name + " : | ";
            for(node a_node : conn_list) {
                output += a_node.name + " |";
            }
            System.out.println(output);
        }
    }
}

