# TCP packet drop analysis

This project consists in the replication of 007, a packet drop cause detection system.
The system was introduced in a conference research paper which reference is provided below.

Note that the working version is located inside the folder **007/version_3**

## Reference
Arzani, B., Ciraci, S., Chamon, L., Zhu, Y., Liu, H. H., Padhye, J., Loo, B. T.,
Outhred, G. (2018). 007: Democratically finding the cause of packet drops. In 15th USENIX Symposium on Networked Systems Design and Implementation (NSDI 18).(pp. 419-435).